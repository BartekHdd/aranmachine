#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdlib.h>

#define GRID_WIDTH 30
#define GRID_HEIGHT 30
#define CELL_SIZE 18 
#define TOTAL_CELLS GRID_WIDTH * GRID_HEIGHT

#define BACK_A 10, 10, 10, 255
#define BACK_B 30, 30, 30, 255
#define MAIN_COLOR 255, 69, 255, 255

typedef struct {int x, y;} coordinates_t;

coordinates_t new_coordinates(int x, int y) { 
	return (coordinates_t) {x, y};}

coordinates_t scale_coordinates(int x, int y) {
	return (coordinates_t) {x / CELL_SIZE, y / CELL_SIZE};
}

int coordinates_to_successive(coordinates_t coordinates) {
	return coordinates.y * GRID_WIDTH + coordinates.x;
}

coordinates_t succesive_to_coordinates(int successive_index) {
	int y = successive_index / GRID_WIDTH;
	return (coordinates_t) {successive_index - (y * GRID_WIDTH), y};
}

void clear_board(bool *board) {
	for(int i = 0; i < TOTAL_CELLS; i++) board[i] = false;
}

bool is_on_board(coordinates_t coordinates) {
	return (coordinates.x >= 0 && coordinates.x < GRID_WIDTH)
	&& (coordinates.y >= 0 && coordinates.y < GRID_HEIGHT);
}

void swap_boards(bool **boards) {
	bool * board = boards[1];
	boards[1] = boards[0];
	boards[0] = board;
}

bool is_populated(bool *board, coordinates_t cell) {
	return board[coordinates_to_successive(cell)];
}

void populate(bool *board, coordinates_t cell) {
	board[coordinates_to_successive(cell)] = true;
}

void depopulate(bool *board, coordinates_t cell) {
	board[coordinates_to_successive(cell)] = false;
}

int count_neighbours(bool *board, coordinates_t cell) {
	int neighbours = 0;
	for (int y = -1; y <= 1; y++) {
		for (int x = -1; x <= 1; x++) {
			if (x == 0 && y == 0) continue;
			coordinates_t test_cell = new_coordinates(cell.x + x, cell.y + y);
			if (is_on_board(test_cell)) {
				if (is_populated(board, test_cell)) neighbours++;
			}
		}
	}
	return neighbours;
}

void tick(bool **boards) {
	clear_board(boards[1]);
	for (int index = 0; index < TOTAL_CELLS; index++) {
		coordinates_t cell = succesive_to_coordinates(index);

		int neighbours = count_neighbours(boards[0], cell);
		bool populated = is_populated(boards[0], cell);

		if (populated && (neighbours < 2 || neighbours > 3))
			depopulate(boards[1], cell);
		else if ((!populated && neighbours == 3) || populated)
			populate(boards[1], cell);
	}
	swap_boards(boards);
}

void render_background(SDL_Renderer *renderer, SDL_Rect *rects) {
	SDL_SetRenderDrawColor(renderer, BACK_A);
	SDL_RenderClear(renderer);
	SDL_SetRenderDrawColor(renderer, BACK_B);
	for (int i = 0; i < TOTAL_CELLS; i++) {
		coordinates_t coordinates = succesive_to_coordinates(i);
		if (coordinates.y % 2 - coordinates.x % 2) {
			SDL_RenderFillRect(renderer, &rects[i]);
		}
	}
}

void render_board(SDL_Renderer *renderer, SDL_Rect *rects, bool *board) {
	SDL_SetRenderDrawColor(renderer, MAIN_COLOR);
	for (int cell = 0; cell < TOTAL_CELLS; cell++) {
		if (board[cell]) {
			SDL_RenderFillRect(renderer, &rects[cell]);
		}
	}
}

int main() {
	bool *boards[2];
	boards[0] = (bool*) malloc(sizeof(bool) * TOTAL_CELLS);
	boards[1] = (bool*) malloc(sizeof(bool) * TOTAL_CELLS);

	SDL_Rect *cells = (SDL_Rect*) malloc(sizeof(SDL_Rect) * TOTAL_CELLS);
	for (int i = 0; i < TOTAL_CELLS; i++) {
		cells[i].w = CELL_SIZE;
		cells[i].h = CELL_SIZE;
		coordinates_t coordinates = succesive_to_coordinates(i);
		cells[i].x = coordinates.x * CELL_SIZE;
		cells[i].y = coordinates.y  * CELL_SIZE;
	}

	SDL_Window *window;
	SDL_Renderer *renderer;
	SDL_CreateWindowAndRenderer(GRID_WIDTH * CELL_SIZE,
			GRID_HEIGHT * CELL_SIZE,
			NULL,
			&window,
			&renderer);

	SDL_Event event;
	char key;
	bool running = true;
	int mouse_x, mouse_y;
	while (running) {
		while (SDL_PollEvent(&event) > 0) {
			switch (event.type)
			{
			case SDL_QUIT:
				running = false;
				break;
			case SDL_MOUSEBUTTONDOWN:
				SDL_GetMouseState(&mouse_x, &mouse_y);
				coordinates_t mouse_coordinates = scale_coordinates(mouse_x, mouse_y);
				int mouse_successive = coordinates_to_successive(mouse_coordinates);
				if (is_populated(boards[0], mouse_coordinates))
					boards[0][mouse_successive] = false;
				else
					boards[0][mouse_successive] = true;
				break;
			case SDL_KEYDOWN:
				key = (char) event.key.keysym.sym;
				if (key == ' ')  tick(boards);
				else if (key == 'c') clear_board(boards[0]);
				break;
			default:
				break;
			}
			render_background(renderer, cells);
			render_board(renderer, cells, boards[0]);
			SDL_RenderPresent(renderer);
		}
	}
	
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
	free(boards[0]);
	free(boards[1]);
	return 0;
}
